-- Set default options for NeoVim:
require "setup.options"
-- Set keymappings
require "setup.keymaps"
-- Load plugins:
require "setup.plugins"
-- Set color scheme
require "setup.colorscheme"
-- Load lualine setup
require "setup.lualine"
-- Setup completion
require "setup.completion"
-- Setup Telescope
require "setup.telescope"
-- Setup Treesitter
require "setup.treesitter"
-- Setup autocommands
require "setup.autocommands"
-- Setup Gitsigns
require "setup.gitsigns"
-- Setup Nvim Tree
require "setup.nvim-tree"
-- Setup bufferline
require "setup.bufferline"
-- Setup Comment plugin
require "setup.comments"
-- Startup greeter screen
require "setup.alpha"
-- Color hightliter for css/html
require("setup.colorizer")
-- Setup which-key plugin. Displays popup window with leader key combos 
require("setup.which-key")
-- Setup LSP
require "setup.lsp"
