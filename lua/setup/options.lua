-- :help options
-- Configure options table:
local o = vim.opt -- define short form for 'vim.opt'
local options = {
    autoindent = true,
    wrap = false, -- display lines as one long line
    mouse = "a", -- allow the mouse to be used in neovim
    history = 1500, -- keep 1500 lines of command line history
    ruler = true, -- show the cursor position all the time
    showcmd = true, -- display incomplete commands
    incsearch = true, -- do incremental searching
    hlsearch = true, -- highlight all matches on previous search pattern
    relativenumber = false, -- set relative numbered lines
    number = true, -- set numbered lines
    numberwidth = 2, -- set number column width to 2 {default 4}
    conceallevel = 0, -- so that `` is visible in markdown files
    signcolumn = "yes", -- always show the sign column, otherwise it would shift the text each time
    listchars = "eol:,tab:>-,trail:~,extends:>,precedes:<,nbsp:%", -- define non-printable characters
    list = true, -- show non-printable characters
    tabstop = 4, -- insert 2 spaces for a tab
    softtabstop = 4,
    shiftwidth = 4, -- the number of spaces inserted for each indentation
    expandtab = true, -- convert tabs to spaces
    smarttab = true,
    ignorecase = true, -- ignore case in search patterns
    smartcase = true, -- case sensitivity will be on if upper case characters are present
    smartindent = true, -- make indenting smarter again
    termguicolors = true, -- enable graphical terminal colors
    clipboard = "unnamedplus", -- allow neovim access to system clipboard
    cursorline = true, -- highlight line with cursor on it
    cursorcolumn = true, -- highlight column with cursor on it
    showtabline = 2, -- always show tabs
    backup = false, -- creates a backup file
    writebackup = false, -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
    swapfile = false, -- creates a swapfile
    cmdheight = 2, -- more space in the neovim command line for displaying messages
    completeopt = { "menuone", "noselect" }, -- mostly just for cmp
    splitbelow = true, -- force all horizontal splits to go below current windows
    splitright = true, -- force all vertical splits to go to the right of current windows
    timeoutlen = 1000, -- time to wait for a mapped sequence to complete (in milliseconds)
    updatetime = 300, -- faster completion (4000ms default)
    undofile = true, -- enable persistent undo
    scrolloff = 8, -- is one of my favour
    sidescrolloff = 8,
    colorcolumn = { 80, 160, 240, 320, 400 }, -- mark specific columns to estimate code line width
}
-- Set all options from table above in a loop
for k, v in pairs(options) do
    o[k] = v
end

vim.g.netrw_banner = 0
vim.g.python3_host_prog = "/home/stannum/.pyenv/versions/nvim-venv/bin/python" -- Instructions: https://github.com/deoplete-plugins/deoplete-jedi/wiki/Setting-up-Python-for-Neovim#tips-for-using-pyenv
