-- vim.cmd('packadd packer.nvim')
local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  }
}

return packer.startup(function(use)
    use "wbthomason/packer.nvim" -- Packer can manage itself
    use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
    use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins
    use "folke/which-key.nvim" -- Displays a popup with possible key bindings of the command you started typing
    use "folke/tokyonight.nvim" -- Color scheme ported from VSCode
    use {
        "rose-pine/neovim", -- Another theme, with plugins support
        as = "rose-pine",
        tag="v1.1.0",
    }
    use {
        "nvim-lualine/lualine.nvim", -- status line written in lua
        requires = { "kyazdani42/nvim-web-devicons", opt = false },
    }
    use {
        "akinsho/bufferline.nvim",  -- bufferline
        tag="v2.12.0",
    }
    use "kyazdani42/nvim-tree.lua" -- Nvim Tree
    use { "https://github.com/numToStr/Comment.nvim", tag="v0.7.0" } -- comment plugin
    use "goolord/alpha-nvim"  -- Startup screen
    use "norcalli/nvim-colorizer.lua" -- css/html hex-color preview
    -- cmp plugins
    use "hrsh7th/nvim-cmp" -- The completion plugin
    use "onsails/lspkind-nvim" -- plugin add source info and icons to completion menu
    use "hrsh7th/cmp-buffer" -- buffer completions
    use "hrsh7th/cmp-path" -- path completions
    use "hrsh7th/cmp-cmdline" -- cmdline completions
    use "hrsh7th/cmp-nvim-lua" -- nvim lua completions
    use "hrsh7th/cmp-nvim-lsp" -- nvim lsp completions
    use "saadparwaiz1/cmp_luasnip" -- snippet completions
    use "petertriho/cmp-git" -- git completion
     -- snippets
    use "L3MON4D3/LuaSnip" --snippet engine
    use "rafamadriz/friendly-snippets" -- a bunch of snippets to use
    -- LSP
    use "neovim/nvim-lspconfig" -- enable LSP
    -- use "williamboman/nvim-lsp-installer" -- language server installer
    use {
        "williamboman/mason.nvim", -- language server installer
        requires = { "williamboman/mason-lspconfig.nvim" }
    }
    use "jose-elias-alvarez/null-ls.nvim" -- plugin for use of linter/formatter and etc, extends LSP
    -- Telescope
    use {
        "nvim-telescope/telescope.nvim", -- Telescope
        requires = { "nvim-telescope/telescope-media-files.nvim", opt = false } -- required Telescope extesion
    }
    -- Treesitter
    use { "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate",
        requires = { "p00f/nvim-ts-rainbow" }, -- Treesitter extesion to colorize parentheses
    }
    -- Git
    use { "lewis6991/gitsigns.nvim", tag="v0.5" }

    -- Make packer installed after git clone
    if PACKER_BOOTSTRAP then
        require("packer").sync()
    end

end)

