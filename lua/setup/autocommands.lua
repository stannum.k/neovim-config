-- All user autocommands here:
local cmd = vim.api.nvim_command
local au_cmd = vim.api.nvim_create_autocmd
cmd([[
  autocmd BufRead * autocmd FileType <buffer> ++once if &ft !~# 'commit\|rebase' && line("'\"") > 1 && line("'\"") <= line("$") | exe 'normal! g`"' | endif
]])

local group = vim.api.nvim_create_augroup("restore_cursor_pos", { clear = true })
au_cmd("FileType", {
    -- TODO: need to utilize this api functions: nvim_win_set_cursor(), nvim_get_mark() vim.fn.line() getpos()
    -- pattern = { 'commit' },
    callback = function ()
        local FileType = vim.fn.expand("<amatch>")
        vim.schedule(function ()
            return 0
            -- print("Detected FileType: " .. vim.inspect(FileType))
        end)
    end,
    desc = 'Restore last cursor position',
    group = group,
})

au_cmd("TextYankPost", {
    desc = "Quick highlight visualization  of yanked text",
    callback = function () vim.highlight.on_yank({ higroup = "IncSearch", timeout = 100 }) end
})

