---
local keymap = vim.api.nvim_set_keymap                        -- set alias for vim api function
local keymap_opts = { noremap = true, silent = true }         -- set options for keymapping

-- Remap space as a Leader key
keymap('', '<Space>', '<Nop>', keymap_opts)                   -- <Space> key to Nop
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
-- default value: vim.g.mapleader = '\\'
-- NeoVim modes:
--   n - normal mode
--   i - insert mode
--   v - visual mode
--   x - visual block mode
--   t - term mode
--   c - command mode

-- Insert mode mappings:
-- Set auto-closing for enclosing substrings
keymap('i', '<Leader>\"', '\"\"<ESC>i', keymap_opts)
keymap('i', '<Leader>\'', '\'\'<ESC>i', keymap_opts)
keymap('i', '<Leader>(', '()<ESC>i', keymap_opts)
keymap('i', '<Leader>[', '[]<ESC>i', keymap_opts)
keymap('i', '<Leader>{', '{}<ESC>i', keymap_opts)

-- Normal mode mappings:
keymap('n', '<S-l>', ':bnext<CR>', keymap_opts)
keymap('n', '<S-h>', ':bprev<CR>', keymap_opts)
keymap("n", "<A-j>", ":move .+1<CR>==", keymap_opts)           -- Move line up
keymap("n", "<A-k>", ":move .-2<CR>==", keymap_opts)           -- Move line down
-- Nvimtree
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", keymap_opts)
-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", keymap_opts)
keymap("n", "<C-Down>", ":resize -2<CR>", keymap_opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", keymap_opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", keymap_opts)
-- Visual mode mappings:
keymap('v', '>', '>gv', keymap_opts)
keymap('v', '<', '<gv', keymap_opts)
keymap("v", "<A-j>", ":move '>+1<CR>gv=gv", keymap_opts)       -- Move block up
keymap("v", "<A-k>", ":move '<-2<CR>gv=gv", keymap_opts)       -- Move block down

-- Telescope key binding:
keymap("n", "<Leader>tf", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", keymap_opts)
keymap("n", "<Leader>tt", "<cmd>Telescope live_grep<cr>", keymap_opts)
