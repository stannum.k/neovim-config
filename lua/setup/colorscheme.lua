vim.g.tokyonight_style = "night" -- other option is "day"
local colorscheme = 'tokyonight'
local rp_status_ok, rose_pine = pcall(require, "rose-pine")
if not rp_status_ok then
   vim.notify("rose-pine color engine not loaded")
else
    rose_pine.setup({
        -- @usage 'main' | 'moon'
        dark_variant = 'moon',
    })
    colorscheme = 'rose-pine'
end

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
    vim.notify("colorscheme " .. colorscheme .. " not found!")
    return
end

