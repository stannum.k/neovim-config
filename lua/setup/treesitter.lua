local ts_status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not ts_status_ok then
  vim.notify("Error loading plugin: treesitter")
  return
end

local opt = vim.opt
opt.foldmethod = "expr"                      -- set folding method to expression based
opt.foldexpr = "nvim_treesitter#foldexpr()"  -- set expression to use TreeSitter

configs.setup {
  ensure_installed = "all",
  sync_install = false,
  ignore_install = { "" }, -- List of parsers to ignore installing
  highlight = {
    enable = true, -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled
    additional_vim_regex_highlighting = true,
  },
  indent = { enable = true, disable = { "yaml" } },
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
    -- colors = {}, -- table of hex strings
    -- termcolors = {} -- table of colour name strings
  }
}
