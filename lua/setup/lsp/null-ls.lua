local plugin = "null-ls"
local status_ok, nullls = pcall(require, plugin)
if not status_ok then
    vim.notify("Error loading plugin: " .. plugin)
    return
end
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
local formatting = nullls.builtins.formatting
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
local diagnostics = nullls.builtins.diagnostics

nullls.setup {
  sources = {
    formatting.prettier.with { extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" } },
    formatting.black.with { extra_args = { "--fast" } },
    formatting.stylua,
    diagnostics.flake8,
  },
}

-- vim: ts=2:sts=2:sw=2
