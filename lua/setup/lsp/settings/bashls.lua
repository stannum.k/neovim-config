return {
  cmd_env = {
    GLOB_PATTERN = "*@(.sh|.inc|.bash|.command)",
  },
  filetypes = {
    "sh",
    "zsh"
  }
}
