local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status_ok then
  vim.notify("Error loading:" .. lspconfig)
  return
end

local mason_lspconfig_status_ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not mason_lspconfig_status_ok then
  vim.notify("Error loading mason-lspconfig!")
  return
end

local installed_servers = require('mason-registry').get_installed_packages()
local servers = {}
for _, server_pkg in ipairs(installed_servers) do
  table.insert(servers,server_pkg.name)
end

mason_lspconfig.setup {
  ensure_installed = servers,
  automatic_installation = true,
}

mason_lspconfig.setup_handlers {
  function (server_name)
    local base_opts = {
      on_attach = require("setup.lsp.handlers").on_attach,
      capabilities = require("setup.lsp.handlers").capabilities,
    }
    -- load settings for LSP server if it exists
    local lsp_opts_ok, lsp_opts = pcall(require, "setup.lsp.settings" .. "." .. server_name)
    if not lsp_opts_ok then
      vim.notify("No configuration for '" .. server_name .. "' found in 'lua/setup/lsp/settings'. Using default settings.")
      lsp_opts = {}
    end
    -- combine base options with LSP-specific options
    local opts = vim.tbl_deep_extend("force", lsp_opts , base_opts)
    -- set configured LSP-servers settings:
    lspconfig[server_name].setup(opts)
  end
}

